#!/bin/bash
#------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/setab: color de fondo
    red=`tput setaf 1`;
    green=`tput setaf 2`;
    blue=`tput setaf 4`;
    bg_blue=`tput setab 4`;
    reset=`tput sgr0`;
    bold=`tput setaf bold`;
#------------------------------------------------------
# VARIABLES GLOBALES
#------------------------------------------------------
proyectoActual="/home/andrew/Documents/repo_GitLab/2013_SoftwareEnginneringAndComplexity_BestPaperAward";
proyectos="/home/andrew/Documents/repo_GitLab/repos.txt";

#------------------------------------------------------
# DISPLAY MENU
#------------------------------------------------------
imprimir_menu () {
       imprimir_encabezado "\t  S  U  P  E  R  -  M  E  N U ";
    
    echo -e "\t\t El proyecto actual es:";
    echo -e "\t\t $proyectoActual";
    
    echo -e "\t\t";
    echo -e "\t\t Opciones:";
    echo "";
    echo -e "\t\t\t a. Mi dirección ip y dirección de red";
    echo -e "\t\t\t b. Dirección de mi router";
    echo -e "\t\t\t c. Dispositivos en la red";
    echo -e "\t\t\t d. Logueo remoto";        
    echo -e "\t\t\t e. Transferencia de archivos ";        
    echo -e "\t\t\t q.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}

#------------------------------------------------------
# FUNCTIONES AUXILIARES
#------------------------------------------------------

imprimir_encabezado () {
    clear;
    #Se le agrega formato a la fecha que muestra
    #Se agrega variable $USER para ver que usuario está ejecutando
    echo -e "`date +"%d-%m-%Y %T" `\t\t\t\t\t USERNAME:$USER";
    echo "";
    #Se agregan colores a encabezado
    echo -e "\t\t ${bg_blue} ${red} ${bold}--------------------------------------\t${reset}";
    echo -e "\t\t ${bold}${bg_blue}${red}$1\t\t${reset}";
    echo -e "\t\t ${bg_blue}${red} ${bold} --------------------------------------\t${reset}";
    echo "";
}

esperar () {
    echo "";
    echo -e "Presione enter para continuar";
    read ENTER ;
}

malaEleccion () {
    echo -e "Selección Inválida ..." ;
}


#------------------------------------------------------
# FUNCTIONES del MENU
#------------------------------------------------------
a_funcion () {
           imprimir_encabezado "\tOpción a.  Mi dirección ip y dirección de red";
	   echo "Su ip/mascara es:"
	   #ip addr | grep "brd" | grep "inet" | cut -c 10-24
	   ip addr show | grep "inet " | grep -v "127.0.0.1/8" | awk '{print $2}'

           IP_ADDR=$(ip addr show | grep "inet " | grep -v "127.0.0.1/8" | awk '{print $2}') 
  	   ./red $IP_ADDR
}

b_funcion () {
           imprimir_encabezado "\tOpción b. Dirección de mi router";
           echo "Su gateway es:"
	   route -n | grep "UG" | awk '{print $2}'
}

c_funcion () {
          imprimir_encabezado "\tOpción c. Dispositivos en la red";
          IP_ADDR=$(ip addr show | grep "inet " | grep -v "127.0.0.1/8" | awk '{print $2}') 
          nmap -sP $IP_ADDR
          printf "\nVerificar conectividad de ip: "
          read mensaje
          ping -w 10 $mensaje;
       
}

d_funcion () {
         imprimir_encabezado "\tOpción d. Logueo remoto";
	 echo "ingrese usuario"
	 read usuario
	 echo "ingrese ip"
	 read ip
	 echo "Ingrese puerto"
	 read puerto
         ssh -X -p $puerto $usuario@$ip xeyes
}


e_funcion () {
         imprimir_encabezado "\tOpción e. Transferir archivo";
	 echo "Ingrese usuario"
	 read usuario
	 echo "ingrese ip"
	 read ip
	 echo "ingrese camino al archivo"
	 read path 
         scp $path $usuario@$ip:Escritorio
    

}



#------------------------------------------------------
# LOGICA PRINCIPAL 
#------------------------------------------------------
while  true
do
    # 1. mostrar el menu
    imprimir_menu;
    # 2. leer la opcion del usuario
    read opcion;
    
    case $opcion in
        a|A) a_funcion;;
        b|B) b_funcion;;
        c|C) c_funcion;;
        d|D) d_funcion;;
        e|E) e_funcion;;
        q|Q) break;;
        *) malaEleccion;;
    esac
    esperar;
done

